package com.book.TopBook.rank;

import com.book.TopBook.book.domain.Book;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * Created by demo094 on 2018-02-17.
 */
public class BookRatings {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private Book book;

	@OneToMany
	private List<Rating> ratings;

	public BookRatings() {

	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}
}
