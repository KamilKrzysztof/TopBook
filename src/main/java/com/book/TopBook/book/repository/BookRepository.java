package com.book.TopBook.book.repository;

import com.book.TopBook.book.domain.Book;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by demo094 on 2018-02-15.
 */
public interface BookRepository extends CrudRepository<Book, Long>{
}
